﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TelevisaoCet30GUI
{
    public partial class Form1 : Form
    {
        private Tv minhaTv;

        public Form1()
        {
            InitializeComponent();
            minhaTv = new Tv();

            LabelStatus.Text = minhaTv.Mensagem;
        }

        private void ButtonOnOff_Click(object sender, EventArgs e)
        {
            
            if(!minhaTv.GetEstado())
            {
                minhaTv.LigaTv();
                LabelStatus.Text = minhaTv.Mensagem;

                ButtonOnOff.Text = "Off";
                ButtonAumentaCanal.Enabled = true;
                ButtonDiminuiCanal.Enabled = true;
                LabelCanal.Text = minhaTv.Canal.ToString();

                TrackBarVolume.Enabled = true;
                LabelVolume.Text = minhaTv.Volume.ToString();
                TrackBarVolume.Value = minhaTv.Volume;
            }
            else
            {
                minhaTv.DesligaTv();
                LabelStatus.Text = minhaTv.Mensagem;
                ButtonOnOff.Text = "On";
                ButtonAumentaCanal.Enabled = false;
                ButtonDiminuiCanal.Enabled = false;
                LabelCanal.Text = ". .";

                TrackBarVolume.Enabled = false;
                LabelVolume.Text = ". .";
            }
            
        }

        private void ButtonAumentaCanal_Click(object sender, EventArgs e)
        {
            minhaTv.Canal ++;
            //minhaTv.MudaCanal(minhaTv.Canal + 1);
            LabelCanal.Text = minhaTv.Canal.ToString();
        }

        private void ButtonDiminuiCanal_Click(object sender, EventArgs e)
        {
            minhaTv.Canal --;
           // minhaTv.MudaCanal(minhaTv.GetCanal() - 1);
            LabelCanal.Text = minhaTv.Canal.ToString();
        }

        private void TrackBarVolume_Scroll(object sender, EventArgs e)
        {

            minhaTv.Volume = TrackBarVolume.Value;
            LabelVolume.Text = minhaTv.Volume.ToString();

        }
    }
}
