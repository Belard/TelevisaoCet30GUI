﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace TelevisaoCet30GUI
{
    public class Tv
    {
        #region Atributos

        private bool estado;
        private int canal;
        private string mensagem;
        private int volume;

        #endregion


        #region Propriedades

        public int Volume { get; set; }

        // public int Volume

        public int Canal
        {
            get { return canal; }

            set {
                    if(value > 0 && value <= 20)
                    {
                        canal = value;
                    }
                }
        }

        public string Mensagem{ get; private set; }


        #endregion


        #region Métodos

        public Tv()
        {
            estado = false;
            Canal = 1;
            Volume = 50;
            Mensagem = "Nova tv criada com sucesso!";            
        }

        //public int GetCanal()
        //{
        //    return canal;
        //}
        //public void MudaCanal(int canal)
        //{
        //    this.canal = canal;
        //}
        //public string EnviaMensagem()
        //{
        //    return mensagem;
        //}

        public void LigaTv()
        {
            if (!estado)
            {
                estado = true;
                LerInfo();
                Mensagem = "Tv ligada!";
            }
        }

        public void DesligaTv()
        {
            if(estado)
            {
                estado = false;
                GravarInfo();
                Mensagem = "Tv desligada!";
            }
        }

        private void GravarInfo()
        {
            string ficheiro = @"tvInfo.txt";
            string linha = Canal + ";" + Volume;

            StreamWriter sw = new StreamWriter(ficheiro, false); // false: não faz append do texto

            if(!File.Exists(ficheiro))
            {
                sw = File.CreateText(ficheiro);
            }
            sw.WriteLine(linha);
            sw.Close();
        }

        private void LerInfo()
        {
            string ficheiro = @"tvInfo.txt";

            StreamReader sr;

            if(File.Exists(ficheiro))
            {
                sr = File.OpenText(ficheiro);
                string linha = "";

                while ((linha = sr.ReadLine())!= null)
                {
                    string[] campos = new string[2];

                    campos = linha.Split(';');

                    Canal = Convert.ToInt32(campos[0]);
                    Volume = Convert.ToInt32(campos[1]);
                }
                sr.Close();
            }
        }

        //public void AumentaVolume(int volume)
        //{
        //    this.volume += volume;
        //}
        //public void DiminuiVolume(int volume)
        //{
        //    this.volume -= volume;
        //}

        //public int GetVolume()
        //{
        //    return volume;
        //}

        //public void SetVolume(int volume)
        //{
        //    this.volume = volume;
        //}

        public bool GetEstado()
        {
            return estado;
        }

        #endregion
    }






}
